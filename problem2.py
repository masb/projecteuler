def fibonacci_seq(below: int) -> list:
    fib_seq = list([1, 2])
    for i in range(0, below-2):
        sum_ = fib_seq[i] + fib_seq[i+1]
        if sum_ >= below:
            break
        else:
            fib_seq.append(sum_)
    return fib_seq


def main(val: int):
    fib_seq = fibonacci_seq(val)
    even_val_seq = list(fib_seq)
    for i in even_val_seq[:]:
        if (i % 2) != 0:
            even_val_seq.remove(i)
    print(even_val_seq)

    sum_even_fib = sum(even_val_seq)
    print(sum_even_fib)


if __name__ == '__main__':
    main(4000000)
