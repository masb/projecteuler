#include <stdio.h>
#include <inttypes.h>

int main(void)
{
	unsigned long long num = 600851475143;
	unsigned long long temp = 1;
	unsigned long long i = 2;
	for(; i < num; i++)
	{
		if(0 == (num % i))
			temp *= i;
		if(temp == num)
			break;
	}
	printf("temp = %llu\n", i);

	return 0;
}
