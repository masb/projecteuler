def largest_prime(num):
    l = []
    for i in range(2, num):
        if 0 == (num % i):
            l.append(i)
            all_ = 1
            for j in l:
                all_ *= j
            if all_ == num:
                break
    return l[-1]


if __name__ == '__main__':
    print(largest_prime(600851475143))
